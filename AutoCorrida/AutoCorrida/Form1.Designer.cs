﻿namespace AutoCorrida
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cboCar = new System.Windows.Forms.ComboBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxKilometros = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnVerificar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.lblMsgLimiteGasolina = new System.Windows.Forms.Label();
            this.trackbarGas = new System.Windows.Forms.TrackBar();
            this.btnViajar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxPrecoGas = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblAlertaCombustivel = new System.Windows.Forms.Label();
            this.lblPrevisaoKm = new System.Windows.Forms.Label();
            this.lblCustoKm = new System.Windows.Forms.Label();
            this.lblTotalKm = new System.Windows.Forms.Label();
            this.lblTotalGastoCombustivel = new System.Windows.Forms.Label();
            this.lblCustoMedioKm = new System.Windows.Forms.Label();
            this.lblAlertaUrgenteCombustivel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackbarGas)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(460, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(230, 144);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // cboCar
            // 
            this.cboCar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCar.FormattingEnabled = true;
            this.cboCar.Location = new System.Drawing.Point(162, 7);
            this.cboCar.Name = "cboCar";
            this.cboCar.Size = new System.Drawing.Size(144, 21);
            this.cboCar.TabIndex = 1;
            this.cboCar.Text = "Selecione o veiculo :";
            this.cboCar.SelectedIndexChanged += new System.EventHandler(this.cboCar_SelectedIndexChanged);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(312, 6);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(142, 122);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Enabled = false;
            this.progressBar1.Location = new System.Drawing.Point(326, 133);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(116, 15);
            this.progressBar1.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(44, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Vai Viajar?";
            // 
            // textBoxKilometros
            // 
            this.textBoxKilometros.Enabled = false;
            this.textBoxKilometros.Location = new System.Drawing.Point(162, 49);
            this.textBoxKilometros.Name = "textBoxKilometros";
            this.textBoxKilometros.Size = new System.Drawing.Size(144, 20);
            this.textBoxKilometros.TabIndex = 5;
            this.textBoxKilometros.TextChanged += new System.EventHandler(this.textBoxKilometros_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Quantos Kms? :";
            // 
            // btnVerificar
            // 
            this.btnVerificar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVerificar.Location = new System.Drawing.Point(502, 209);
            this.btnVerificar.Name = "btnVerificar";
            this.btnVerificar.Size = new System.Drawing.Size(87, 33);
            this.btnVerificar.TabIndex = 7;
            this.btnVerificar.Text = "Verificar";
            this.btnVerificar.UseVisualStyleBackColor = true;
            this.btnVerificar.Click += new System.EventHandler(this.btnVerificar_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(4, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(147, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "Quantos Litros? :";
            // 
            // lblMsgLimiteGasolina
            // 
            this.lblMsgLimiteGasolina.AutoSize = true;
            this.lblMsgLimiteGasolina.Location = new System.Drawing.Point(108, 121);
            this.lblMsgLimiteGasolina.Name = "lblMsgLimiteGasolina";
            this.lblMsgLimiteGasolina.Size = new System.Drawing.Size(10, 13);
            this.lblMsgLimiteGasolina.TabIndex = 10;
            this.lblMsgLimiteGasolina.Text = "-";
            // 
            // trackbarGas
            // 
            this.trackbarGas.Enabled = false;
            this.trackbarGas.Location = new System.Drawing.Point(162, 83);
            this.trackbarGas.Maximum = 100;
            this.trackbarGas.Name = "trackbarGas";
            this.trackbarGas.Size = new System.Drawing.Size(144, 45);
            this.trackbarGas.TabIndex = 1;
            this.trackbarGas.Scroll += new System.EventHandler(this.trackbarGas_Scroll);
            // 
            // btnViajar
            // 
            this.btnViajar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViajar.Location = new System.Drawing.Point(502, 289);
            this.btnViajar.Name = "btnViajar";
            this.btnViajar.Size = new System.Drawing.Size(87, 28);
            this.btnViajar.TabIndex = 12;
            this.btnViajar.Text = "Viajar";
            this.btnViajar.UseVisualStyleBackColor = true;
            this.btnViajar.Click += new System.EventHandler(this.btnViajar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(4, 209);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(349, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Previsao de Kms per nivel do Tanque de combustivel Atual :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(266, 229);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Custo viajem :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(221, 249);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Total Kms da viatura :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(164, 269);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(189, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Alerta Reserva de Combustivel :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(10, 128);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(141, 20);
            this.label8.TabIndex = 17;
            this.label8.Text = "Preço Gasolina :";
            // 
            // textBoxPrecoGas
            // 
            this.textBoxPrecoGas.Location = new System.Drawing.Point(162, 127);
            this.textBoxPrecoGas.Name = "textBoxPrecoGas";
            this.textBoxPrecoGas.Size = new System.Drawing.Size(144, 20);
            this.textBoxPrecoGas.TabIndex = 18;
            this.textBoxPrecoGas.TextChanged += new System.EventHandler(this.textBoxPrecoGas_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(148, 289);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(205, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = " Valor total gasto em combustível :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(125, 309);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(228, 13);
            this.label10.TabIndex = 20;
            this.label10.Text = "Calcular o custo médio por quilómetro :";
            // 
            // lblAlertaCombustivel
            // 
            this.lblAlertaCombustivel.AutoSize = true;
            this.lblAlertaCombustivel.ForeColor = System.Drawing.Color.Red;
            this.lblAlertaCombustivel.Location = new System.Drawing.Point(360, 268);
            this.lblAlertaCombustivel.Name = "lblAlertaCombustivel";
            this.lblAlertaCombustivel.Size = new System.Drawing.Size(0, 13);
            this.lblAlertaCombustivel.TabIndex = 21;
            // 
            // lblPrevisaoKm
            // 
            this.lblPrevisaoKm.AutoSize = true;
            this.lblPrevisaoKm.Location = new System.Drawing.Point(360, 209);
            this.lblPrevisaoKm.Name = "lblPrevisaoKm";
            this.lblPrevisaoKm.Size = new System.Drawing.Size(0, 13);
            this.lblPrevisaoKm.TabIndex = 22;
            // 
            // lblCustoKm
            // 
            this.lblCustoKm.AutoSize = true;
            this.lblCustoKm.Location = new System.Drawing.Point(360, 229);
            this.lblCustoKm.Name = "lblCustoKm";
            this.lblCustoKm.Size = new System.Drawing.Size(0, 13);
            this.lblCustoKm.TabIndex = 23;
            // 
            // lblTotalKm
            // 
            this.lblTotalKm.AutoSize = true;
            this.lblTotalKm.Location = new System.Drawing.Point(360, 248);
            this.lblTotalKm.Name = "lblTotalKm";
            this.lblTotalKm.Size = new System.Drawing.Size(0, 13);
            this.lblTotalKm.TabIndex = 24;
            // 
            // lblTotalGastoCombustivel
            // 
            this.lblTotalGastoCombustivel.AutoSize = true;
            this.lblTotalGastoCombustivel.Location = new System.Drawing.Point(360, 290);
            this.lblTotalGastoCombustivel.Name = "lblTotalGastoCombustivel";
            this.lblTotalGastoCombustivel.Size = new System.Drawing.Size(0, 13);
            this.lblTotalGastoCombustivel.TabIndex = 25;
            // 
            // lblCustoMedioKm
            // 
            this.lblCustoMedioKm.AutoSize = true;
            this.lblCustoMedioKm.Location = new System.Drawing.Point(360, 309);
            this.lblCustoMedioKm.Name = "lblCustoMedioKm";
            this.lblCustoMedioKm.Size = new System.Drawing.Size(0, 13);
            this.lblCustoMedioKm.TabIndex = 26;
            // 
            // lblAlertaUrgenteCombustivel
            // 
            this.lblAlertaUrgenteCombustivel.AutoSize = true;
            this.lblAlertaUrgenteCombustivel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlertaUrgenteCombustivel.ForeColor = System.Drawing.Color.Red;
            this.lblAlertaUrgenteCombustivel.Location = new System.Drawing.Point(46, 171);
            this.lblAlertaUrgenteCombustivel.Name = "lblAlertaUrgenteCombustivel";
            this.lblAlertaUrgenteCombustivel.Size = new System.Drawing.Size(0, 16);
            this.lblAlertaUrgenteCombustivel.TabIndex = 27;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 356);
            this.Controls.Add(this.lblAlertaUrgenteCombustivel);
            this.Controls.Add(this.lblCustoMedioKm);
            this.Controls.Add(this.lblTotalGastoCombustivel);
            this.Controls.Add(this.lblTotalKm);
            this.Controls.Add(this.lblCustoKm);
            this.Controls.Add(this.lblPrevisaoKm);
            this.Controls.Add(this.lblAlertaCombustivel);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBoxPrecoGas);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnViajar);
            this.Controls.Add(this.trackbarGas);
            this.Controls.Add(this.lblMsgLimiteGasolina);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnVerificar);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxKilometros);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.cboCar);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackbarGas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox cboCar;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxKilometros;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnVerificar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblMsgLimiteGasolina;
        private System.Windows.Forms.TrackBar trackbarGas;
        private System.Windows.Forms.Button btnViajar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxPrecoGas;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblAlertaCombustivel;
        private System.Windows.Forms.Label lblPrevisaoKm;
        private System.Windows.Forms.Label lblCustoKm;
        private System.Windows.Forms.Label lblTotalKm;
        private System.Windows.Forms.Label lblTotalGastoCombustivel;
        private System.Windows.Forms.Label lblCustoMedioKm;
        private System.Windows.Forms.Label lblAlertaUrgenteCombustivel;
    }
}

