﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AutoCorrida
{
    public partial class Form1 : Form
    {
        FormulaUm F1 = new FormulaUm();
        public Form1()
        {
            InitializeComponent();
            cboCar.Items.AddRange(new string[] { "Ferrari", "Jeep", "Ford" });
        }

        private void cboCar_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboCar.Text == "Ferrari")
            {
                pictureBox1.Image = Image.FromFile(@"...\image\F3.jpg");
                F1.CapacidadeTotal = 95;
                F1.ConsumoMedio = 18;
                F1.KmTotal = 0;
                F1.Matricula = "MD-LD-98";
            }
            if (cboCar.Text == "Jeep")
            {
                pictureBox1.Image = Image.FromFile(@"...\image\F2.jpg");
                F1.CapacidadeTotal = 93;
                F1.ConsumoMedio = 9.7;
                F1.KmTotal = 0;
                F1.Matricula = "MX-ED-56";
            }
            if (cboCar.Text == "Ford")
            {
                pictureBox1.Image = Image.FromFile(@"...\image\F1.jpg");
                F1.CapacidadeTotal = 85;
                F1.ConsumoMedio = 4.7;
                F1.KmTotal = 0;
                F1.Matricula = "AL-FM-03";
            }
            textBoxKilometros.Enabled = true;
        }

        private void textBoxKilometros_TextChanged(object sender, EventArgs e)
        {
            F1.Viagens = Convert.ToDouble(textBoxKilometros.Text);
            trackbarGas.Enabled = true;
        }

        private void trackbarGas_Scroll(object sender, EventArgs e)
        {
            progressBar1.Enabled = true;
            double deposito = 0.0d;
            deposito = F1.CapacidadeTotal * trackbarGas.Value / 100;
            F1.Conteudo = deposito;
            F1.ValorEmPercentagem();
            progressBar1.Value = F1.ValorEmPercentagem();
        }

        private void textBoxPrecoGas_TextChanged(object sender, EventArgs e)
        {
            F1.CustoLitro = Convert.ToDouble(textBoxPrecoGas.Text);
        }

        private void btnVerificar_Click(object sender, EventArgs e)
        {
            F1.Consumo();
            lblPrevisaoKm.Text = Convert.ToString(F1.PrevisaoKm);
            F1.CalculoCustoKilometro();
            lblCustoKm.Text = Convert.ToString(F1.CustoKm * F1.Viagens);
            F1.CalculoCustoKilometro();
            lblCustoMedioKm.Text = Convert.ToString(F1.CustoKm);
            F1.CalculoCustoTotal();
            lblTotalGastoCombustivel.Text = Convert.ToString(F1.CustoTotal);
            F1.AtualizaCombustivel();
            AtivarViagem();
        }
        private void AtivarViagem()
        { 

            if (Convert.ToDouble(textBoxKilometros.Text) > Convert.ToDouble(lblPrevisaoKm.Text))
            {
                lblAlertaUrgenteCombustivel.Text = F1.ToString();
                btnViajar.Enabled = false;
                btnVerificar.Enabled = true;
            }
            else if (Convert.ToDouble(lblPrevisaoKm.Text) < 10)
            {
                lblAlertaCombustivel.Text = F1.ToString();
                btnViajar.Enabled = false;
                btnVerificar.Enabled = true;
            }
            else
            {
                lblAlertaCombustivel.Visible = false;
                lblAlertaUrgenteCombustivel.Visible = true;
                btnVerificar.Enabled = true;
            }
            btnViajar.Enabled = true;
            
        }
        private void btnViajar_Click(object sender, EventArgs e)
        {
            btnVerificar.Enabled = false;
            progressBar1.Value = Convert.ToInt32(F1.AtualizaCombustivel());

            if (progressBar1.Value < 0)
            {
                btnViajar.Enabled = false;
                btnVerificar.Enabled = true;
            }
            else
            {
                F1.Consumo();
                lblPrevisaoKm.Text = Convert.ToString(F1.PrevisaoKm);
                F1.CalculoCustoKilometro();
                lblCustoKm.Text = Convert.ToString(F1.CustoKm * F1.Viagens);
                F1.AtualizaKmVeiculo();
                lblTotalKm.Text = Convert.ToString(F1.KmTotal);
                F1.CalculoCustoKilometro();
                lblCustoMedioKm.Text = Convert.ToString(F1.CustoKm);
                F1.CalculoCustoTotal();
                lblTotalGastoCombustivel.Text = Convert.ToString(F1.CustoTotal);
                progressBar1.Value = F1.ValorEmPercentagem();

                if (progressBar1.Value < 10)
                {
                    lblAlertaCombustivel.Visible = true;
                    lblAlertaCombustivel.Text = F1.ToString();
                    btnVerificar.Enabled = true;
                    btnViajar.Enabled = false;
                }
                else
                {
                    if (Convert.ToDouble(textBoxKilometros.Text) > Convert.ToDouble(lblPrevisaoKm.Text))
                    {
                        lblAlertaUrgenteCombustivel.Visible = true;
                        lblAlertaUrgenteCombustivel.Text = F1.ToString();
                        btnVerificar.Enabled = true;
                        btnViajar.Enabled = false;
                    }
                    if (Convert.ToDouble(lblPrevisaoKm.Text) < 10)
                    {
                        lblAlertaCombustivel.Visible = true;
                        lblAlertaCombustivel.Text = F1.ToString();
                        btnVerificar.Enabled = true;
                        btnViajar.Enabled = false;
                    }
                }
            }
        }
    }
}
