﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoCorrida
{
    class FormulaUm
    {
        #region atributos
        private string _matricula;
        private double _kmTotal;
        private double _capacidadeTotal;
        private double _Conteudo;
        private double _consumoMedio;
        private double _viagens;
        private double _custoLitro;
        private double _previsaoKm;
        private double _custoKm;
        private double _custoTotal;
        #endregion

        #region propriedades
        public string Matricula { get; set; }
        public double KmTotal { get; set; }
        public double CapacidadeTotal { get; set; }
        public double Conteudo { get; set; }
        public double ConsumoMedio { get; set; }
        public double Viagens { get; set; }
        public double CustoLitro { get; set; }
        public double PrevisaoKm { get; set; }
        public double CustoKm { get; set; }
        public double CustoTotal { get; set; }
        #endregion

        #region Construtores
        //public FormulaUm()
        //{
        //    Matricula = "12-00-MD";
        //    CapacidadeTotal = 120;
        //    KmTotal = 100500;
        //    Conteudo = 100;
        //    ConsumeMedio = 4.5;
        //    Viagens = 200.40;
        //    CustoLitro = 1.23;
        //    PrevisaoKm = 10;
        //    CustoKm = 1.4;
        //    CustoTotal = 25.5;
        //}

        public FormulaUm() : this("", 120, 100500, 100, 4.5, 200.40, 1.23, 10, 1.4, 25.50) { }

        public FormulaUm(string matricula, double kmtotal, double capacidadeTotal, double conteudo, double consumoMedio, double viagens, double custoLitro, double previsaoKm, double custoKm, double custoTotal)
        {
            Matricula = matricula;
            KmTotal = kmtotal;
            CapacidadeTotal = capacidadeTotal;
            Conteudo = conteudo;
            ConsumoMedio = consumoMedio;
            Viagens = viagens;
            CustoLitro = custoLitro;
            PrevisaoKm = previsaoKm;
            CustoKm = custoKm;
            CustoTotal = custoTotal;
        }
        public FormulaUm(FormulaUm FU) : this(FU.Matricula, FU.KmTotal, FU.CapacidadeTotal, FU.Conteudo, FU.ConsumoMedio, FU.Viagens, FU.CustoLitro, FU.PrevisaoKm, FU.CustoKm, FU.CustoTotal)
        {

        }
        #endregion

        #region Metodos Gerais
        public override string ToString()
        {
            return String.Format("A viatura com a matricula: {0}, Não chegara ao destino FALTA DE COMBUSTIVEL", Matricula);
        }
        #endregion

        #region Outros Metodos

        public double Consumo()
        {
            PrevisaoKm = (Conteudo * 100) / ConsumoMedio;
            return PrevisaoKm;
        }

        public double AtualizaKmVeiculo()
        {
            KmTotal = Viagens + KmTotal;
            return KmTotal;
        }

        public bool ReservaCombustivel()
        {
            if (Conteudo < 10)
            {
                return true;
            }
            else
                return false;
        }

        public double CalculoCustoTotal()
        {
            CustoTotal = CapacidadeTotal * CustoLitro;
            return CustoTotal;
        }

        public double CalculoCustoKilometro()
        {
            CustoKm = (ConsumoMedio / 100) * CustoLitro;
            return CustoKm;
        }
        public double AtualizaCombustivel()
        {
            Conteudo = ((PrevisaoKm - Viagens) * ConsumoMedio) / 100;
            return Conteudo;
        }
        public int ValorEmPercentagem()
        {
            return Convert.ToInt32(Math.Round(Conteudo / CapacidadeTotal * 100));
        }
        #endregion
    }
}
